﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour {
    public static AudioManager Instance;
    public AudioSource Aud;
    public float randTimer;
    public AudioClip[] noises;
    public AudioClip startNoise;
	// Use this for initialization
	void Start () {
        Instance = this;
        Aud.PlayOneShot(startNoise, 1);
        randTimer = Random.Range(7, 25);
    }
	
	// Update is called once per frame
	void Update () {
        randTimer -= Time.deltaTime;
        if(randTimer <= 0)
        {
            PlayNoise();
        }
	}

    void PlayNoise()
    {
        randTimer = Random.Range(7, 25);
        var choice = Random.Range(0, noises.Length);
        Aud.PlayOneShot(noises[choice], 1);
    }
}
