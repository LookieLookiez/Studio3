﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Radio : MonoBehaviour {
    public GameObject LT;
    public GameObject HJ;
    public float timer;
    public AudioSource radioSource;
    
	// Use this for initialization
	void Start () {
        timer = Random.Range(1, 3);
	}
	
	// Update is called once per frame
	void Update () {
        timer -= Time.deltaTime;

        if(timer <= 0)
        {
            radioSource.Play();
        }

        if(radioSource.isPlaying)
        {
            LT.GetComponent<Monster>().monsterSpeed = 2;
            HJ.GetComponent<Monster>().monsterSpeed = 2;
        }
	}

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Vision"))
        {
            if(radioSource.isPlaying)
            {
                print("looking at radio and is playing");
                if (Input.GetKey(KeyCode.E))
                {
                    radioSource.Stop();
                    timer = Random.Range(15, 60);
                    LT.GetComponent<Monster>().monsterSpeed = 1;
                    HJ.GetComponent<Monster>().monsterSpeed = 1;
                }
            }
        }
    }
}
