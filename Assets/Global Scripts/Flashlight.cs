﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flashlight : MonoBehaviour {
    public static Flashlight Instance;

    public Light flashlight;
    private float batteries;
    private float max = 20;
    public AudioSource Aud;
    public AudioClip windUp;
    public AudioClip TorchAudio;
    public bool TorchIsOn;
    public float windValue;
    private float winding;

    //used when blanket is being used
    public bool CanBeUsed = true;

    // Use this for initialization
    void Start () {
        Instance = this;
        flashlight.enabled = false;
        batteries = max;
        TorchIsOn = false;
	}
	
	// Update is called once per frame
	void Update () {

        if(batteries > 0)
        {
            if(CanBeUsed)
            {

                if (Input.GetKey(KeyCode.F))
                {
                    flashlight.enabled = true;
                    batteries -= Time.deltaTime * 2;
                    if (!TorchIsOn)
                    {
                        batteries -= 0.5f;
                        Aud.PlayOneShot(TorchAudio, 1);
                        TorchIsOn = true;
                    }
                    else
                    {
                        return;
                    }
                }
                else
                {
                    flashlight.enabled = false;
                }

                if (Input.GetKeyUp(KeyCode.F))
                {
                    TorchIsOn = false;
                    Aud.PlayOneShot(TorchAudio, 0.5f);
                }
            }
            
        }

        else
        {
            flashlight.enabled = false;
        }

        if(batteries < max)
        {
            if (Input.GetKeyDown(KeyCode.R))
            {

                if (winding > 0)
                {
                    return;
                }
                Aud.PlayOneShot(windUp, 1);
                batteries += windValue;
                winding = 1;
            }
            if (winding > 0)
            {
                winding -= Time.deltaTime;
            }
        }
        if(batteries > max)
        {
            batteries = max;
        }
        
        
            //print(batteries);
    }
}
