﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterController : MonoBehaviour {

    // requires mouse controlled vision
    // requies mouse look clamp so that the player can't turn their head all around or look into the bed
    // requires raycast(?) to allow categories of "looking at" 
    // this will pass to manster controller -"if looking at"
    // requires "listen" button input that is active only when the audio starts

    Vector2 mouseLook;
    Vector2 smoothV;
    public float sensitivity = 5.0f;
    public float smoothness = 2.0f;

    //Added clamp variables as public float to change in editior
    public float minYClamp = -20f;
    public float maxYClamp = 120f;
    public float minXClamp = -100f;
    public float maxXClamp = 45f;

    // This stops the code from making calculations it doesn't have to for giant, ridiculous numbers.
    public float maxPeripheral = 700;
    public float minPeripheral = 100;

    public float minPercentage = 0f;
    public float maxPercentage = 1f;

    private float lerpDist;
    public AnimationCurve MonsterFadeCurve;
    public bool pauseCam;
    GameObject head;

    #region Singleton
    //making this into a singleton
    public static CharacterController Instance;
    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(this);
        }
        if (Instance == null)
        {
            Instance = this;
        }
    }
    #endregion

    #region Object References

    // this is going to be hacky by having each enemy be dropped into the designations in the unity inspector. If I was going to do this properly I would have it look for anything with a "monster" tag.


    #endregion

    void Start () {
        head = this.transform.parent.gameObject;
        lerpDist = maxPercentage - minPercentage;
        Cursor.visible = false;
        pauseCam = false;

    }
	
	void Update ()
    {
        UpdateCamera();
    }

    void UpdateCamera()
    {
        Vector2 visionCone;
        if (pauseCam)
        {
            visionCone = new Vector2(0, 0);
        }
        else
        {
            visionCone = new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y"));
        }
        

        visionCone = Vector2.Scale(visionCone, new Vector2(sensitivity * smoothness, sensitivity * smoothness));
        smoothV.x = Mathf.Lerp(smoothV.x, visionCone.x, 1f / smoothness);
        smoothV.y = Mathf.Lerp(smoothV.y, visionCone.y, 1f / smoothness);
        mouseLook += smoothV;
        // this clamp angle up/down should allow the player to look at the end of the bed and their feet or up on the wall above them.
        mouseLook.y = Mathf.Clamp(mouseLook.y, minYClamp, maxYClamp);
        //This should allow the player to look just beyonf the demon in the corner and the projected image on the wall.
        mouseLook.x = Mathf.Clamp(mouseLook.x, minXClamp, maxXClamp);

        transform.localRotation = Quaternion.AngleAxis(-mouseLook.y, Vector3.right);
        head.transform.localRotation = Quaternion.AngleAxis(mouseLook.x, head.transform.up);
    }
}
