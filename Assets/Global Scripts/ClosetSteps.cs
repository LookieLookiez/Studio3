﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClosetSteps : MonoBehaviour {
    public GameObject doorLeft;
    public GameObject doorRight;
    private int step1 = 0;
    private int step2 = 15;
    private int step3 = 25;
    private int step4 = 90;
    public enum States { SAFE, WARNING, CLOSE, DEATH};
    public States currentState;
    public GameObject monster;
    private float monsterTimer;
    private float monsterTimerMax = 50;
    private int flashCount;
    public AudioSource Aud;
    public AudioClip doorClose;





    // Use this for initialization
    void Start () {
        monsterTimer = monsterTimerMax;
        
	}
	
	// Update is called once per frame
	void Update () {
        ClosetMonster();
        switch (currentState)
        {
            case States.SAFE:
                doorLeft.transform.rotation = Quaternion.Euler(90, 90, 180);
                doorRight.transform.rotation = Quaternion.Euler(90, 90, 180);
                monster.active = false;

                break;
            case States.WARNING:
                doorLeft.transform.rotation = Quaternion.Euler(90, 90, 180);
                doorRight.transform.rotation = Quaternion.Euler(90, 90, 190);
                monster.active = false;

                break;
            case States.CLOSE:
                doorLeft.transform.rotation = Quaternion.Euler(90, 90, 180);
                doorRight.transform.rotation = Quaternion.Euler(90, 90, 210);
                monster.active = true;

                break;
            case States.DEATH:
                doorLeft.transform.rotation = Quaternion.Euler(90, 90, 90);
                doorRight.transform.rotation = Quaternion.Euler(90, 90, -90);
                monster.active = false;

                break;
            default:


                break;
        }
    }

    void ClosetMonster()
    {
        monsterTimer -= Time.deltaTime;
        if (monsterTimer > 30)
        {
            currentState = States.SAFE;
        }
        if (monsterTimer < 30 && monsterTimer > 20)
        {
            currentState = States.WARNING;
        }
        if (monsterTimer < 20 && monsterTimer > 10)
        {
            currentState = States.CLOSE;
        }
        if (monsterTimer < 0)
        {
            currentState = States.DEATH;
           // GameManager.Instance.Jumpscare();
            UIManager.Instance.ChangeScenes(0);
        }

    }
    /*
    private void OnTriggerStay(Collider other)
    {
        if(other.CompareTag("Vision"))
        {
            print("We Collided");
            if(Flashlight.Instance.TorchIsOn == true && monsterTimer < 20)
            {
                Debug.Log("Flashing!");
                flashCount++;
                if(flashCount > 100)
                {
                    monsterTimer = monsterTimerMax;
                    Aud.PlayOneShot(doorClose, 1);
                    flashCount = 0;
                }
                
            }
        }
    }
    */
}
