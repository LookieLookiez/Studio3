﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BedMonster : MonoBehaviour {

    
    [Header("Models")]
    public GameObject BlanketModel;
    //public GameObject BedMonsterModel;
    //public GameObject BedMonsterArm;

    [Header("Locations")]
    public Transform BlanketStartPos;
    public Transform BlanketEndPos;

    [Header("Variables")]
    public bool PlayerIsTugging = false;
    public float JumpscareTimeMax;
    public float TimeToJumpscare;
    float BlanketTravelTime;


	// Use this for initialization
	void Start ()
    {
        //reset the timer values
        TimeToJumpscare = JumpscareTimeMax;

        BlanketModel.transform.position = BlanketStartPos.transform.position;
	}
	
	// Update is called once per frame
	void Update ()
    {

        if (Input.GetButton("Fire1"))
        {
            Flashlight.Instance.CanBeUsed = false;

            if (Flashlight.Instance.TorchIsOn == false)
            {
                PlayerIsTugging = true;
                Debug.Log("TUGGING");
                CharacterController.Instance.pauseCam = true;
            }

            else if (Flashlight.Instance.TorchIsOn == true)
            {
                PlayerIsTugging = false;
            }
        }

        else
        {
            PlayerIsTugging = false;
            Flashlight.Instance.CanBeUsed = true;
            CharacterController.Instance.pauseCam = false;
        }

        



        //if player is pressing mouse down
        if (PlayerIsTugging)
        {
            Flashlight.Instance.CanBeUsed = false;
            if(TimeToJumpscare < JumpscareTimeMax)
            {
                TimeToJumpscare += Time.deltaTime / 2;
                BlanketMovement();
            }
            
        }

        else
        {
            //make timer tick down
            TimeToJumpscare -= Time.deltaTime;

            BlanketMovement();

            Flashlight.Instance.CanBeUsed = true;

            if (TimeToJumpscare <= 0)
            {
                Debug.LogError("JUMPSCARE");
                TimeToJumpscare = 0;
            }
        }
    }

    void BlanketMovement()
    {
        BlanketModel.transform.position = Vector3.Lerp(BlanketEndPos.position, BlanketStartPos.position, TimeToJumpscare/100f *2.7f);
    }
}
