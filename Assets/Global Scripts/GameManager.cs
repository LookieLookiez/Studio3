﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    private float winTimer = 300; // 5 Minutes

    public static GameManager Instance;
    public GameObject jumpscare_1;

    // Use this for initialization
    void Start()
    {
        Instance = this;
      //  jumpscare_1.SetActive(false);
        Invoke("CountDown", 0);
    }

    // Update is called once per frame
    void Update()
    {
      if(winTimer >= 0)
        {
            Win();
        }
    }

    public void LTJumpscare()
    {
        jumpscare_1.SetActive(true);
    }

    public void HJJumpscare()
    {
        jumpscare_1.SetActive(true);
    }

    public void BedJumpscare()
    {
        jumpscare_1.SetActive(true);
    }

    public void Win()
    {

    }
    public void Lose()
    {
        SceneManager.LoadScene(0);
    }

    void CountDown()
    {
        winTimer -= Time.deltaTime;
    }

}
