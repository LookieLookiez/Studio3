﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Monster : MonoBehaviour {
    public GameObject monsterTransform;

    public float monsterTimer;

    public GameObject state1Model;
    public GameObject state2Model;
    public GameObject state3Model;
    public GameObject doorLeft;
    public GameObject doorRight;
    public AudioClip secondStateSound;
    private bool secondPlayed = false;
    public AudioClip thirdStateSound;
    private bool thirdPlayed = false;
    public AudioClip fourthStateSound;
    private bool fourthPlayed = false;

    public enum MonsterType { LongTooth, HorseJerk, BedMonster };
    public MonsterType thisMonster;

    public enum States { SAFE, WARNING, CLOSE, DEATH };
    public States currentState;

    public float monsterTimerMax;
    private int flashCount;

    public AudioSource Aud;
    public AudioClip doorClose;
    public AudioClip HJSeen;
    private bool played = false;
    public Animator HJAnim;
    public Animator jumpscares;
    public GameObject jumpscaresImage;

    public float monsterSpeed;

    // Use this for initialization
    void Start () {
        var newTime = Random.Range(30, monsterTimerMax);
        monsterTimer = newTime;
        
    }
	
	// Update is called once per frame
	void Update () {
        InitiateMonster();
        switch (currentState)
        {
            case States.SAFE:
                monsterTransform = state1Model;

                if(thisMonster == MonsterType.LongTooth)
                {
                    doorLeft.transform.rotation = Quaternion.Euler(90, 90, 180);
                    doorRight.transform.rotation = Quaternion.Euler(90, 90, 180);
                    monsterTransform.SetActive(false);
                }

                if (thisMonster == MonsterType.HorseJerk)
                {
                    HJAnim.SetInteger("state", 0);
                }
                secondPlayed = false;
                thirdPlayed = false;
                fourthPlayed = false;

                break;
            case States.WARNING:
                monsterTransform = state2Model;

                if (secondPlayed)
                {
                    return;
                }
                else
                {
                    AudioManager.Instance.Aud.PlayOneShot(secondStateSound, 1);
                    secondPlayed = true;
                }

                if (thisMonster == MonsterType.LongTooth)
                {
                    doorLeft.transform.rotation = Quaternion.Euler(90, 90, 180);
                    doorRight.transform.rotation = Quaternion.Euler(90, 90, 190);
                    monsterTransform.SetActive(false);
                }

                if (thisMonster == MonsterType.HorseJerk)
                {
                    HJAnim.SetInteger("state", 1);
                }

                break;
            case States.CLOSE:
                monsterTransform = state3Model;

                if (thirdPlayed)
                {
                    return;
                }
                else
                {
                    AudioManager.Instance.Aud.PlayOneShot(thirdStateSound, 1);
                    thirdPlayed = true;
                }

                if (thisMonster == MonsterType.LongTooth)
                {
                    doorLeft.transform.rotation = Quaternion.Euler(90, 90, 180);
                    doorRight.transform.rotation = Quaternion.Euler(90, 90, 210);
                    monsterTransform.SetActive(true);
                }

                if (thisMonster == MonsterType.HorseJerk)
                {
                    HJAnim.SetInteger("state", 2);
                }

                break;
            case States.DEATH:

                if (fourthPlayed)
                {
                    return;
                }
                else
                {
                    AudioManager.Instance.Aud.PlayOneShot(fourthStateSound, 1);
                    fourthPlayed = true;
                }

                if (thisMonster == MonsterType.LongTooth)
                {
                    doorLeft.transform.rotation = Quaternion.Euler(90, 90, 90);
                    doorRight.transform.rotation = Quaternion.Euler(90, 90, -90);
                    monsterTransform.SetActive(false);
                    jumpscaresImage.SetActive(true);
                    jumpscares.SetInteger("Jumpscare", 1);
                }

                if (thisMonster == MonsterType.HorseJerk)
                {
                    HJAnim.SetInteger("state", 3);
                    jumpscaresImage.SetActive(true);
                    jumpscares.SetInteger("Jumpscare", 2);
                }

                //GameManager.Instance.Lose();

                break;
            default:


                break;
        }
    }

    private void ResetHorseJerk()
    {
        var newTime = Random.Range(30, monsterTimerMax);
        monsterTimer = newTime;
        monsterTransform.SetActive(false);
        played = false;
    }

    private void ResetLongtooth()
    {
        var newTime = Random.Range(30, monsterTimerMax);
        monsterTimer = newTime;
        played = false;
        Aud.PlayOneShot(doorClose, 1);
        currentState = States.SAFE;
    }

    void InitiateMonster()
    {
        monsterTimer -= Time.deltaTime * monsterSpeed;
        if (monsterTimer > 30)
        {
            currentState = States.SAFE;
        }
        if (monsterTimer < 30 && monsterTimer > 20)
        {
            currentState = States.WARNING;
        }
        if (monsterTimer < 20 && monsterTimer > 10)
        {
            currentState = States.CLOSE;
        }
        if (monsterTimer < 0)
        {
            currentState = States.DEATH;
           // UIManager.Instance.ChangeScenes(0);
        }

    }
    //Flashlight to close cupboard
    private void OnTriggerStay(Collider other)
    {
        if (thisMonster == MonsterType.LongTooth)
        {
            if (other.CompareTag("Vision"))
            {
                print("We Collided");
                if (Flashlight.Instance.TorchIsOn == true && monsterTimer < 20)
                {
                    Debug.Log("Flashing!");
                    flashCount++;
                    if (flashCount > 100)
                    {
                        Invoke("ResetLongtooth", 0.2f);
                        var newTime = Random.Range(25, monsterTimerMax);
                        monsterTimer = newTime;
                        flashCount = 0;
                    }

                }
            }
        }

        if (thisMonster == MonsterType.HorseJerk)
        {
            if (other.CompareTag("Vision"))
            {
                print("HJ Collided");
                if (Flashlight.Instance.TorchIsOn == true && monsterTimer < 20)
                {
                    print("HJ seen!");
                    monsterTransform.SetActive(true);
                    Invoke("ResetHorseJerk", 0.2f);

                    if (played)
                    {
                        return;
                    }
                    else {
                        Aud.PlayOneShot(HJSeen, 1);
                        played = true;
                    }
                }
            }
        }
    }

}

