﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {
    public static UIManager Instance;

    [Header("Options")]
    public GameObject OptionsPanel;
    public Toggle InvertY;
    public Button BackToMenu;

    [Header("Credits")]
    public GameObject CreditsPanel;

    // Use this for initialization
    void Start () {
        Instance = this;
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKey(KeyCode.Escape))
        {
            Application.Quit();
        }
	}

    public void ChangeScenes(int scene)
    {
        SceneManager.LoadScene(scene);
    }

    public void OnOptions()
    {
        OptionsPanel.SetActive(true);
    }

    public void OnCredits()
    {
        CreditsPanel.SetActive(true);
    }

    public void OnBackToMenu()
    {
        OptionsPanel.SetActive(false);
        CreditsPanel.SetActive(false);
    }

    public void Kickstart()
    {
        Application.OpenURL("https://www.kickstarter.com/");
    }
}
